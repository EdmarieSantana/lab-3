package sortersTesterClasses;

import sorterClasses.BubbleSortSorter;

public class Entero implements Comparable<Entero> { 
       private int value; 
       public Entero(int v) { value = v; }
       public int getValue() { return value; } 
       public String toString() { return value + ""; }
       public int compareTo(Entero other) { 
          if (other == null) throw new IllegalArgumentException();
          return this.value - other.value;  
       } 
       
       public static void Test() {
    	   Entero[] ESC = {new Entero(10), new Entero(5), new Entero(80), new Entero(1), new Entero(0), new Entero(9), new Entero(2)};
    	   BubbleSortSorter<Entero> ElemHeroBubleSort = new BubbleSortSorter<>();
    	   System.out.println("Unsorted: ");
    	   for (int i = 0; i < ESC.length; i++) {
    		   System.out.print(ESC[i]+ " ");
			
		}
    	   ElemHeroBubleSort.sort(ESC, null);
    	   System.out.println();
    	   System.out.println("Sorted: ");
    	   for (int i = 0; i < ESC.length; i++) {
    		   System.out.print(ESC[i]+ " ");
			
		}
       }
       public static void main (String[] args){
    	   Test();
       }
       
    }
